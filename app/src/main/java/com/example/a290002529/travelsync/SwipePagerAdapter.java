package com.example.a290002529.travelsync;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class SwipePagerAdapter extends PagerAdapter {

    private final Context context;
    private final List<Card> cards;

    public SwipePagerAdapter(final Context context) {
        this.context = context;
        cards = new ArrayList<>();
    }


    public void setCards(final List<Card> cards) {
        if (cards != null && !cards.isEmpty()) {
            this.cards.clear();                    // Clear current cards.
            this.cards.addAll(cards);              // Add new cards.
        }

        notifyDataSetChanged();                    // Notify Pager to display changes.
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View parent = inflater.inflate(R.layout.activity_custom_swipe, container, false);
        final ImageView imageView = (ImageView) parent.findViewById(R.id.swip_image_view);
        final TextView textView = (TextView) parent.findViewById(R.id.imageCount);

        final Card card = cards.get(position);
        final int file = card.getFile();
        imageView.setImageResource(file);
        textView.setText(card.getName() + " Credit Card" + "\nAvailable Credit: $" +
                String.format("%.2f", (card.getLimit() - card.getBalance())) +
                        " \nCurrent Balance: $ " + String.format("%.2f", card.getBalance()));

        container.addView(parent);
        return parent;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }

    @Override
    public int getCount() {
        return cards.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
