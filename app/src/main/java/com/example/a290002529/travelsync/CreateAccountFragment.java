package com.example.a290002529.travelsync;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class CreateAccountFragment extends Fragment implements View.OnClickListener {

    public static CreateAccountFragment newInstance() {
        Bundle args = new Bundle();

        CreateAccountFragment fragment = new CreateAccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Button createAccountButton;
    private User newUser;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.create_account_fragment, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createAccountButton = (Button) view.findViewById(R.id.createAccountButton);
        createAccountButton.setOnClickListener(this);

        // TODO - SETUP VIEWS.

        // TODO - PASS IN USER TYPED INFO IN NEW USER OBJECT.
        newUser = new User("name", "username", "password", 0, 0);
    }


    @Override
    public void onClick(View v) {
        AccountManager.INSTANCE.setUser(newUser);
        final Intent startNewActivity = new Intent(getActivity(), BaseNav.class);
        startActivity(startNewActivity);
    }
}
