package com.example.a290002529.travelsync;

import java.util.ArrayList;

/**
 *  TripManager.
 */
public enum TripManager {

    INSTANCE;

    private ArrayList<Trip> trips;

    public ArrayList<Trip> getTrip() {
        return trips;
    }

    public void setTrip(ArrayList<Trip> trips) {
        this.trips = trips;
    }

}
