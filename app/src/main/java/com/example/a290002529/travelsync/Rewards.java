package com.example.a290002529.travelsync;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.a290002529.travelsync.Api.PARAMS;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class Rewards extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PARAMS {



    public User u;
    int gold = Color.rgb(251, 198, 0);
    int green = Color.rgb(110, 191, 74);
    int blue = Color.rgb(52, 101, 127);
    int red = Color.rgb(198, 53, 9);
    int white = Color.rgb(255, 255, 255);
    public int counter = -1;
    private RelativeLayout mainLayout;
    private PieChart mChart;
    public int totalRewards;
    public double rewardsCash;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards);

        u = AccountManager.INSTANCE.getUser();

        ArrayList<Integer> yData = new ArrayList<Integer>();

        for (int i = 0; i < u.getWallet().size(); i++){
            totalRewards = totalRewards + u.getCard(i).getRewards();
        }

        for (int j = 0; j < u.getWallet().size(); j++){
            yData.add(u.getCard(j).getRewards());
        }


        mChart = (PieChart) findViewById(R.id.chart1);
        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);

        List<PieEntry> entries = new ArrayList<PieEntry>();

        for (Integer data : yData) {
            counter = counter + 1;
            // turn your data into Entry objects
            entries.add(new PieEntry(data, u.getCard(counter).getName()));
        }

        PieDataSet dataSet = new PieDataSet(entries, null); // add entries to dataset

        ArrayList<Integer> colors = new ArrayList<Integer>();
        ArrayList<String> labels = new ArrayList<String>();

        colors.add(gold);
        colors.add(green);
        colors.add(blue);
        colors.add(red);

        for (int k = 0; k < u.getCardCount(); k++)
            labels.add(u.getCard(k).getName());

        dataSet.setColors(colors);
        Legend legend = mChart.getLegend();
        legend.setEnabled(false);

               PieData pieData = new PieData(dataSet);
        pieData.setValueTextSize(20f);
        pieData.setValueTextColor(white);
        dataSet.setSliceSpace(5);
        dataSet.setSelectionShift(5);


        String centerText = "Total Rewards \n" + Integer.toString(totalRewards);

        rewardsCash = totalRewards * 0.05;

        TextView textView = (TextView) findViewById(R.id.rewardsInfo);
        textView.setText("Current Cash Rewards: $"+ String.format("%.2f", rewardsCash));

        mChart.getDescription().setEnabled(false);
        mChart.setCenterText(centerText);
        mChart.setCenterTextColor(white);
        mChart.setUsePercentValues(false);
        mChart.setData(pieData);
        mChart.setHoleRadius(40);
        mChart.setTransparentCircleRadius(42);
        mChart.setHoleColor(1);

        mChart.invalidate(); // refresh

        setupNav();



    }


    private void setupNav() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            final int fragmentCount = getSupportFragmentManager().getBackStackEntryCount();
            if (fragmentCount > 1) {
                getSupportFragmentManager().popBackStackImmediate();
            } else {
                getSupportFragmentManager().popBackStackImmediate();
                super.onBackPressed();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.base_nav, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_rewards) {
            Intent startNewActivity = new Intent(this, Rewards.class);
            startActivity(startNewActivity);
        } else if (id == R.id.nav_payments) {
            Intent startNewActivity = new Intent(this, SyPI.class);
            startActivity(startNewActivity);
        } else {
            goToFragment(id, new Bundle());
        }

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void goToFragment(final int id, final Bundle bundle) {

        if (id == R.id.nav_cards ) {
            goToCardSwipeScreen();

        } else if (id == R.id.nav_payments) {
            goToPaymentScreen();

        } else if (id == R.id.nav_booking) {
            goToBooking();

        }
    }

    private void goToPaymentScreen() {
        addFragment(PaymentsScreen.newInstance());
    }


    private void goToBooking() {
        addFragment(BookingScreen.newInstance());
    }


    private void goToCardSwipeScreen() {
        addFragment(CardSwipeScreen.newInstance());
    }


    private <T extends Fragment> void addFragment(final T fragment) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final String fragmentName = fragment.getClass().getName();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack( fragmentName )
                .commit();
    }

    public void flyScreen(View view) {
        Intent startNewActivity = new Intent(this, Fly.class);
        startActivity(startNewActivity);
    }


    public void driveScreen(View view) {
        Intent startNewActivity = new Intent(this, Drive.class);
        startActivity(startNewActivity);
    }


    public void sleepScreen(View view) {
        Intent startNewActivity = new Intent(this, Sleep.class);
        startActivity(startNewActivity);
    }


    public void cardLookup(View view) {
        Intent startNewActivity = new Intent(this, CardLookup.class);
        startActivity(startNewActivity);
    }


    public void sendSyPI(View view) {
        Intent startNewActivity = new Intent(this, SyPI.class);
        startActivity(startNewActivity);
    }


    public void addCard(View view) {
        Intent startNewActivity = new Intent(this, AddCard.class);
        startActivity(startNewActivity);
    }

    public void sendRewards(View view) {
        Intent startNewActivity = new Intent(this, Rewards.class);
        startActivity(startNewActivity);
    }



}