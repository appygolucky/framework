package com.example.a290002529.travelsync;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.example.a290002529.travelsync.Api.ApiService;
import com.example.a290002529.travelsync.Api.Flight.TripOption;
import com.example.a290002529.travelsync.Api.Flight.Trips;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Fly extends AppCompatActivity {

    private Context context;
    private View loadingView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fly);
        context = this;

        ImageButton checkInBtn = (ImageButton) findViewById(R.id.imageButton3);
        final EditText returnEditText = (EditText) findViewById(R.id.selectDatePickup);
        final CalendarDatePickerDialogFragment.OnDateSetListener checkInOnDateSetListener = new CalendarDatePickerDialogFragment.OnDateSetListener() {
            @Override
            public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
                returnEditText.setText(monthOfYear+1 + "/" + dayOfMonth + "/" + year);
            }
        };
        checkInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOL","WHY U NO WORK?!");
                CalendarDatePickerDialogFragment checkInCDP = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(checkInOnDateSetListener)
                        .setFirstDayOfWeek(Calendar.SUNDAY);
                checkInCDP.show(getSupportFragmentManager(), "CHECKIN_CDP");

            }
        });



        ImageButton checkOutBtn = (ImageButton) findViewById(R.id.imageButton);
        final EditText checkOutEditText = (EditText) findViewById(R.id.editText4);
        final CalendarDatePickerDialogFragment.OnDateSetListener checkOutOnDateSetListener = new CalendarDatePickerDialogFragment.OnDateSetListener() {
            @Override
            public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
                checkOutEditText.setText(monthOfYear+1 + "/" + dayOfMonth + "/" + year);
            }
        };
        checkOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOL","WHY U NO WORK?!");
                CalendarDatePickerDialogFragment checkOutCDP = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(checkOutOnDateSetListener)
                        .setFirstDayOfWeek(Calendar.SUNDAY);
                checkOutCDP.show(getSupportFragmentManager(), "CHECKOUT_CDP");

            }
        });

        loadingView = findViewById(R.id.loadingView);
        loadingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /** Disable click when loading view is visible. */
            }
        });
    }


    //send to fly selection screen onClick
    public void sendFlySelection(View view) {
        TextView originView = (TextView) findViewById(R.id.editText2);
        String origin = originView.getText().toString();

        TextView destinationView = (TextView) findViewById(R.id.editText5);
        String destination = destinationView.getText().toString();

        TextView startDateView = (TextView) findViewById(R.id.selectDatePickup);
        String startDate = startDateView.getText().toString();

        TextView endDateView = (TextView) findViewById(R.id.editText4);
        String endDate = endDateView.getText().toString();

        loadingView.setVisibility(View.VISIBLE);
        ApiService service = new ApiService(context);
        service.makeFlightRequest(origin, destination, "2016-12-20", "2016-12-25", new ApiService.Callback<Trips>() {
            @Override
            public void onFinish(Trips result) {

                loadingView.setVisibility(View.GONE);
                ArrayList<Trip> trips = convert(result);
                TripManager.INSTANCE.setTrip(trips);
                Intent startNewActivity = new Intent(context, FlySelection.class);
                startActivity(startNewActivity);
            }
        });
    }


    private ArrayList<Trip> convert(Trips tripsJson)
    {
        ArrayList<Trip> trips = new ArrayList<>();
        List<TripOption> tripOption = tripsJson.getTrips().getTripOption();
        for(int i = 0; i < tripOption.size(); i++) {
            Trip t = new Trip();
            String carrier = tripsJson.getTrips().getData().getCarrier().get(0).getName();
            t.setCarrier(carrier);
            if(carrier.contains("Delta")) {
                t.setFlightIcon(ContextCompat.getDrawable(this, R.mipmap.delta));
            }
            else if (carrier.contains("Jetblue")) {
                t.setFlightIcon(ContextCompat.getDrawable(this, R.mipmap.jetblue));
            }
            else if (carrier.contains("Southwest")) {
                t.setFlightIcon(ContextCompat.getDrawable(this, R.mipmap.southwest));
            }
            else {
                t.setFlightIcon(ContextCompat.getDrawable(this, R.mipmap.american));
            }
            String priceString = tripOption.get(i).getSaleTotal();
            String pricePiece = priceString.substring(3);
            Double tripPrice = Double.parseDouble(pricePiece);
            for(int j = 0; j < tripOption.get(i).getSlice().size(); j++) {
                FlightObject f = new FlightObject();
                int layovers = 0;
                int duration = 0;
                // Double price = tripOption.get(i).getSlice().get(j).getSegment().get(j).getLeg().get(j).get;
                String departureLoc = tripOption.get(i).getSlice().get(0).getSegment().get(0).getLeg().get(0).getOrigin();
                String arrivalLoc = tripOption.get(i).getSlice().get(0).getSegment().get(0).getLeg().get(0).getDestination();

                if(j == tripOption.get(i).getSlice().size() - 1) {
                    t.setOrigin(departureLoc);
                }
                if(j == 0) {
                    t.setDestination(arrivalLoc);
                }
                f.setArrivalLoc(arrivalLoc);
                f.setDeptLoc(departureLoc);
            }
            t.setPrice(tripPrice);
            trips.add(t);
        }
        return trips;
    }
}
