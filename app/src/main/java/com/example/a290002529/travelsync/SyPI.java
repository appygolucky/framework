package com.example.a290002529.travelsync;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.synchronyfinancial.plugin.SynchronyPlugIn;
import com.synchronyfinancial.plugin.SynchronyPlugInFragment;

public class SyPI extends AppCompatActivity {

    //SyPI Console
    //url: https://console.gpshopper.com/
    //username: bd@syf.com
    //password: fCRWJFpj

    private static final int YOUR_CLIENT_ID = 289;
    private static final String YOUR_CLIENT_NAME = "SyPI BD";
    private static final String YOUR_CLIENT_KEY = "OHccHRdrSxzQoNTG2PNlZYhUC2eGR48b";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sy_pi);

        SynchronyPlugIn.getInstance().initialize(this, YOUR_CLIENT_ID,
                YOUR_CLIENT_NAME, YOUR_CLIENT_KEY);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_sy_pi, new SynchronyPlugInFragment())
                .commit();
    }
}
