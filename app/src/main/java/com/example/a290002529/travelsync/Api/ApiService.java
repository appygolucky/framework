package com.example.a290002529.travelsync.Api;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.a290002529.travelsync.Api.Flight.Trips;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.UnsupportedEncodingException;

public class ApiService {
    private final int MY_SOCKET_TIMEOUT_MS = 20000;

    private final RequestQueue requestQueue;

    public ApiService(final Context context) {
        requestQueue = Volley.newRequestQueue(context);
    }


    public void makeFlightRequest(String origin, String dest, String startTime, String endTime, final Callback<Trips> callback) {
        final String url = "https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyD3PSsq2Z12m0KupeXWcniORHMVkoSEBX8";
        final String body =
                "{\n" +
                "  \"request\": {\n" +
                "    \"passengers\": {\n" +
                "      \"adultCount\": 1\n" +
                "    },\n" +
                "    \"slice\": [\n" +
                "      {\n" +
                "        \"origin\": \"" + origin + "\",\n" +
                "        \"destination\": \"" + dest + "\",\n" +
                "        \"date\": \"" + startTime + "\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"origin\": \"" + dest + "\",\n" +
                "        \"destination\": \"" + origin + "\",\n" +
                "        \"date\": \"" + endTime + "\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"solutions\": 10\n" +
                "  }\n" +
                "}";

        makeStringRequest(url, body, callback);
    }


    //
    public void makeStringRequest(final String url, final String body, final Callback<Trips> callback) {
        final Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Trips data = deserialize(response, Trips.class);
                callback.onFinish(data);
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFinish(null);
            }
        };


        final StringRequest sr = new StringRequest(Request.Method.POST, url, responseListener, errorListener) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return body == null ? null : body.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                            body,
                            "utf-8");

                    return null;
                }
            }

        };

        sr.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(sr);
    }


    /**
     * Parses Json into specified Object.
     */
    public <T> T deserialize(String json, Class<T> clazz) {
        final GsonBuilder builder = new GsonBuilder();
        final Gson gson = builder.create();
        return gson.fromJson(json, clazz);
    }


    /**
     * Callback interface
     */
    public interface Callback<T> {
        void onFinish(final T result);
    }

}
