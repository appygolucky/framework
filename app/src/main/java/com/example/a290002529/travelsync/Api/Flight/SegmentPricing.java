
package com.example.a290002529.travelsync.Api.Flight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SegmentPricing {

    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("fareId")
    @Expose
    private String fareId;
    @SerializedName("segmentId")
    @Expose
    private String segmentId;

    /**
     * 
     * @return
     *     The kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * 
     * @param kind
     *     The kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * 
     * @return
     *     The fareId
     */
    public String getFareId() {
        return fareId;
    }

    /**
     * 
     * @param fareId
     *     The fareId
     */
    public void setFareId(String fareId) {
        this.fareId = fareId;
    }

    /**
     * 
     * @return
     *     The segmentId
     */
    public String getSegmentId() {
        return segmentId;
    }

    /**
     * 
     * @param segmentId
     *     The segmentId
     */
    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

}
