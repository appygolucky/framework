package com.example.a290002529.travelsync;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.a290002529.travelsync.Api.PARAMS;

import java.util.ArrayList;

public class LoginScreen extends AppCompatActivity implements PARAMS {
    // ArrayList of User objects
    ArrayList<User> users = new ArrayList<>();
    public User activeUser;
    // A flag to designate if we are testing or not
    // NOTE: Change this to false before distribution
    boolean testState = true;

    private TextView createAccountView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        createUsers();

        createAccountView = (TextView) findViewById(R.id.createAccountView);
        createAccountView.setOnClickListener(createAccountOnClickListener);
    }


    public void sendLogin(View view){
        TextView usernameView = (TextView)findViewById(R.id.etUsername);
        String username = usernameView.getText().toString();
        TextView passwordView = (TextView)findViewById(R.id.etPassword);
        String password = passwordView.getText().toString();
        activeUser = users.get(0);
        if (checkCredentials(username, password)) {
            Intent startNewActivity = new Intent(this, BaseNav.class);
            Bundle b = new Bundle();
            startNewActivity.putExtra(USER_KEY, activeUser);
            AccountManager.INSTANCE.setViewId(R.id.nav_booking);
            startNewActivity.putExtras(b);

            AccountManager.INSTANCE.setUser(activeUser);
            startActivity(startNewActivity);
        }
        else {
            showSimplePopUp();
        }
    }


    private void showSimplePopUp(){
        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        //sets title, message, and button text for alert dialog
        helpBuilder.setTitle("Login Failed");
        helpBuilder.setMessage("Your Username or Password did not match, please try again");
        helpBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener(){
            //sets action of alert dialog
            public void onClick(DialogInterface dialog, int which) {
            //close dialog
            }
        });
        //shows the dialog
        AlertDialog helpdialog = helpBuilder.create();
        helpdialog.show();
    }


    /**
     * creates users and populates arraylist for them
     */
    private void createUsers()
    {
        User user0 = new User("Jake Miller", "millertime", "password", 100, 2);
        User user1 = new User("Michael Kimble", "kimblemj", "passw0rd", 101, 3);
        User user2 = new User("Julian Peck", "peckj", "pass", 102, 0);

        users.add(user0);
        users.add(user1);
        users.add(user2);
    }


    public boolean checkCredentials(String username, String password)
    {
        for (User user:users) {
            if(username.equals(user.getUsername()) && password.equals(user.getPassword())) {
                activeUser = user;
                return true;
            }
        }
        return testState;
    }


    private View.OnClickListener createAccountOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // TODO - GO TO CREATE ACCOUNT.

        }
    };
}