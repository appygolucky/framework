package com.example.a290002529.travelsync;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by 290002528 on 11/15/2016.
 */


public class Card implements Parcelable {
    private String[] names = {"Cathay Pacific", "Hertz", "Stash Hotel", "Synchrony Financial"};
    private int[] files = {R.drawable.cathay, R.drawable.hertz, R.drawable.stash , R.drawable.syf};

    private String name;
    private int file;
    private double balance;
    private double limit;
    private int rewards;

    public Card(int id, double balance, int limit, int rewards)
    {
        this.name = names[id];
        this.file = files[id];
        this.balance = balance;
        this.limit  = limit;
        this.rewards = rewards;
    }

    /**
     * Method to get user's selected card name
     * @return card name from card object
     */
    public String getName() {return name;}

    /**
     * Method to get balance on user's card
     * @return balance amount from card object
     */
    public Double getBalance() {return balance;}

    /**
     * Method to add balance on user's card
     * @return balance amount from card object
     */
    public Double addBalance(double value) {return value + balance;}

    /**
     * Method to get credit limit on user's card
     * @return credit limit from card object
     */
    public Double getLimit() {return limit;}

    /**
     * Method to add rewards to card
     * @param value amount of rewards points to be added
     * @return updated rewards value with new points added
     */
    public Double addLimit(int value) {return value + limit;}

    /**
     * Method to remove limit frp, card
     * @param value amount of rewards points to be added
     * @return updated rewards value with new limit subtracted
     */
    public Double subtractLimit(int value) {return value - limit;}

    /**
     * Method to get rewards information on user's card
     * @return rewards from card object
     */
    public int getRewards() {return rewards;}

    /**
     * Method to add rewards to card
     * @param value amount of rewards points to be added
     * @return updated rewards value with new points added
     */
    public int addRewards(int value) {return value + rewards;}

    /**
     * Method to remove rewards to card
     * @param value amount of rewards points to be added
     * @return updated rewards value with new points subtracted
     */
    public int subtractRewards(int value) {return value - rewards;}




    public int getFile() {return file;}


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(this.names);
        dest.writeIntArray(this.files);
        dest.writeString(this.name);
        dest.writeInt(this.file);
        dest.writeDouble(this.balance);
        dest.writeDouble(this.limit);
        dest.writeInt(this.rewards);
    }

    protected Card(Parcel in) {
        this.names = in.createStringArray();
        this.files = in.createIntArray();
        this.name = in.readString();
        this.file = in.readInt();
        this.balance = in.readDouble();
        this.limit = in.readDouble();
        this.rewards = in.readInt();
    }

    public static final Creator<Card> CREATOR = new Creator<Card>() {
        @Override
        public Card createFromParcel(Parcel source) {
            return new Card(source);
        }

        @Override
        public Card[] newArray(int size) {
            return new Card[size];
        }
    };
}
