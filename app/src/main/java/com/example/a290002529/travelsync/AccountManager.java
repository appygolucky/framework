package com.example.a290002529.travelsync;

/**
 *  AccountManager.
 */
public enum AccountManager {

    INSTANCE;

    private User user;

    private int viewId;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getViewId() { return viewId; }

    public void setViewId(int viewId) { this.viewId = viewId; }


}
