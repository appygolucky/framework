package com.example.a290002529.travelsync;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

/**
 * Created by 290002528 on 11/29/2016.
 */

public class CarObject extends ItineraryObject {
    private String model;
    private String make;
    private int year;
    private int rentalDate;
    private int returnDate;
    private Drawable carIcon;


    public CarObject(){
        //Empty Constructor
    }

    public Drawable getCarIcon() {
        return carIcon;
    }

    public void setCarIcon(Drawable carIcon) {
        this.carIcon = carIcon;
    }

    public CarObject(double price, String id, int rentalDate, int returnDate, String model, String make, int year, Drawable carIcon){
    super(price,id);
this.rentalDate=rentalDate;
    this.returnDate=returnDate;
    this.model=model;
    this.make=make;
    this.year=year;
    this.carIcon = carIcon;
}

    public int getRentalDate() {return rentalDate;}

    public void setRentalDate(int rentalDate) {this.rentalDate= rentalDate;}

    public int getReturnDate() {return returnDate;}

    public void setReturnDate(int returnDate){this.returnDate = returnDate;}

    public int getYear(){return year;}

    public void setYear(int year){this.year=year;}

    public String getMake(){return make;}

    public void setMake(String make){this.make=make;}

    public String getModel() {return model;}

    public void setModel(String model){this.model=model;}
}
