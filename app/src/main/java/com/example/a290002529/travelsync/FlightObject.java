package com.example.a290002529.travelsync;

import android.os.Parcelable;

/**
 * Created by 290002528 on 11/29/2016.
 */

public class FlightObject extends Trip implements Parcelable{
    private int departTime;
    private int arrivalTime;
    private int duration;
    private String deptLoc;
    private String arrivalLoc;

    public FlightObject()
    {
        // empty constructor
    }

    public FlightObject(double price, String id, int departTime, int arrivalTime, int duration, String deptLoc, String arrivalLoc)
    {
        super(price, id);
        this.departTime = departTime;
        this.arrivalTime = arrivalTime;
        this.duration = duration;
        this.deptLoc = deptLoc;
        this.arrivalLoc = arrivalLoc;
    }

    public int getDepartTime() {
        return departTime;
    }

    public void setDepartTime(int departTime) {
        this.departTime = departTime;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDeptLoc() {
        return deptLoc;
    }

    public void setDeptLoc(String deptLoc) {
        this.deptLoc = deptLoc;
    }

    public String getArrivalLoc() {
        return arrivalLoc;
    }

    public void setArrivalLoc(String arrivalLoc) {
        this.arrivalLoc = arrivalLoc;
    }
}

