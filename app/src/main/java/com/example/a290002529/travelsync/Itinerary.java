package com.example.a290002529.travelsync;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/**
 * Created by 290002528 on 11/29/2016.
 */

public class Itinerary implements Parcelable {

    private ArrayList<ItineraryObject> items;

    public Itinerary()
    {
        items = new ArrayList<ItineraryObject>();
    }

    public void addItem(ItineraryObject item)
    {
        items.add(item);
    }

    public void removeItem(ItineraryObject item)
    {
        items.remove(item);
    }

    public ArrayList<ItineraryObject> getItems()
    {
        return items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.items);
    }

    protected Itinerary(Parcel in) {
       this.items = in.createTypedArrayList(ItineraryObject.CREATOR);
    }

    public static final Creator<Itinerary> CREATOR = new Creator<Itinerary>() {
        @Override
        public Itinerary createFromParcel(Parcel source) {
            return new Itinerary(source);
        }

        @Override
        public Itinerary[] newArray(int size) {
            return new Itinerary[size];
        }
    };
}
