package com.example.a290002529.travelsync;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;


public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.ViewHolder> {


    private ArrayList<Trip> mDataset;
    private RadioButton lastCheckedRadioButton = null;
    private int lastCheckedPosition = -1;


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RadioButton Flight_radButton;
        public TextView Flight_info;
        public ImageView Flight_icon;

        public ViewHolder(View v) {
            super(v);
            Flight_icon = (ImageView) v.findViewById(R.id.flight_logo);
            Flight_info = (TextView) v.findViewById(R.id.flight_info);
            Flight_radButton = (RadioButton) v.findViewById(R.id.flight_radButton);
        }
    }


    public FlightAdapter(ArrayList<Trip> myDataset) {
        mDataset = myDataset;
    }


    public void add(int position, FlightObject item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }


    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }


    public Trip getSelectedFlight() {
        if (lastCheckedPosition > -1) {
            return mDataset.get(lastCheckedPosition);
        }

        return null;
    }


    @Override
    public FlightAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.flight_recycler_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        FlightAdapter.ViewHolder vh = new FlightAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(FlightAdapter.ViewHolder holder, final int position) {
        final Trip trip = mDataset.get(position);

        // TODO - GET / SET Flight INFO
        String FlightDescription = "$ " + trip.getPrice()+ "\n"
                + "Departure airport: " + trip.getOrigin() + "\n" +
                "Arrival Airport: " + trip.getDestination() + "\n" ;
        holder.Flight_info.setText(FlightDescription);

        // TODO - GET Flight ICON
        Drawable image;

        holder.Flight_icon.setImageDrawable(trip.getFlightIcon());


        holder.Flight_radButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final boolean lastPositionWasChecked = (lastCheckedRadioButton != null);
                if (lastPositionWasChecked) {
                    lastCheckedRadioButton.setChecked(false);
                }

                final RadioButton clickedRadioButton = ((RadioButton) v);
                clickedRadioButton.setChecked(true);
                lastCheckedRadioButton = clickedRadioButton;
                lastCheckedPosition = position;

            }
        });

    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}