
package com.example.a290002529.travelsync.Api.Flight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Trips {

    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("trips")
    @Expose
    private Trips_ trips;

    /**
     * 
     * @return
     *     The kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * 
     * @param kind
     *     The kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * 
     * @return
     *     The trips
     */
    public Trips_ getTrips() {
        return trips;
    }

    /**
     * 
     * @param trips
     *     The trips
     */
    public void setTrips(Trips_ trips) {
        this.trips = trips;
    }

}
