package com.example.a290002529.travelsync;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

/**
 * Created by 290002528 on 12/8/2016.
 */

public class Trip extends ItineraryObject {

    private ArrayList<FlightObject> flights;

    public Drawable getFlightIcon() {
        return flightIcon;
    }

    public void setFlightIcon(Drawable flightIcon) {
        this.flightIcon = flightIcon;
    }

    private Drawable flightIcon;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    private String origin;

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    private String destination;

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    private String carrier = "";

    public Trip() { }

    public Trip(Double price, String id) {
        super(price, id);
        flights = new ArrayList<>();
    }

    public ArrayList<FlightObject> getFlights() { return flights; }

    public void addFlight(FlightObject flight)
    {
        flights.add(flight);
    }

}
