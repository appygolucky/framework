package com.example.a290002529.travelsync;

import android.graphics.drawable.Drawable;

/**
 * Created by 290002528 on 11/29/2016.
 */

public class HotelObject extends ItineraryObject{
    private String name;
    private int checkInTime;
    private int checkOutTime;
    private int stayDuration;
    private String hotelLocation;
    private Drawable hotelImage;

    public HotelObject(){
        //empty constructor
    };



    public HotelObject(double price, String id, String name, int checkInTime, int checkOutTime, String hotelLocation, int stayDuration, Drawable hotelImage){
        super (price,id);
        this.checkInTime= checkInTime;
        this.checkInTime= checkOutTime;
        this.stayDuration=stayDuration;
        this.hotelLocation=hotelLocation;
        this.name=name;
        this.hotelImage=hotelImage;
    };

    public double getPrice() {return price;}

    public String getName() {return name;}

    public int getCheckInTime() {return checkInTime;}

    public void setCheckInTime(int checkInTime) {this.checkInTime= checkInTime;}

    public int getCheckOutTime() {return checkOutTime;}

    public void setCheckOutTime(int checkOutTime){this.checkOutTime = checkOutTime;}

    public int getStayDuration(){return stayDuration;}

    public void setStayDuration(int stayDuration){this.stayDuration=stayDuration;}

    public String getHotelLocation(){return hotelLocation;}

    public void setHotelLocation(String hotelLocation){this.hotelLocation=hotelLocation;}

    public Drawable getHotelImage() {
        return hotelImage;
    }

    public void setHotelImage(Drawable hotelImage) {
        this.hotelImage = hotelImage;
    }
}
