package com.example.a290002529.travelsync;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.a290002529.travelsync.Api.PARAMS;

public class BaseNav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PARAMS {

    public User activeUser;
    public String rewards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_nav);

        Bundle bundle = getIntent().getExtras();

        setupNav();

        activeUser = AccountManager.INSTANCE.getUser();


        final int CARD_ID = (bundle == null) ? R.id.nav_booking : AccountManager.INSTANCE.getViewId();
        goToFragment(CARD_ID, bundle);
    }

    private void setupNav() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            final int fragmentCount = getSupportFragmentManager().getBackStackEntryCount();
            if (fragmentCount > 1) {
                getSupportFragmentManager().popBackStackImmediate();
            } else {
                getSupportFragmentManager().popBackStackImmediate();
                super.onBackPressed();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.base_nav, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showRewarwdsPopUp(){
        AlertDialog.Builder helpbuilder = new AlertDialog.Builder(this);
        helpbuilder.setTitle("Wait!");
        helpbuilder.setMessage("It looks like you don't have any credit cards added yet. Please add a credit card to display rewards information.");
        helpbuilder.setPositiveButton("Return to Booking", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //close back to card activity
            }
        });
        helpbuilder.setNegativeButton("Add Credit Card", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent startNewActivity = new Intent(getApplicationContext(), AddCard.class);
                startActivity(startNewActivity);
            }
        });
        AlertDialog helpdialog = helpbuilder.create();
        helpdialog.show();
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (activeUser.getWallet().size() == 0 && id == R.id.nav_cards){
            Intent startNewActivity = new Intent(this, AddCard.class);
            startActivity(startNewActivity);
        } else if (activeUser.getWallet().size() == 0 && id == R.id.nav_rewards ) {
            showRewarwdsPopUp();
        } else if (id == R.id.nav_rewards) {
            Intent startNewActivity = new Intent(this, Rewards.class);
            startActivity(startNewActivity);
        } else if (id == R.id.nav_payments) {
            Intent startNewActivity = new Intent(this, SyPI.class);
            startActivity(startNewActivity);
        } else if (id == R.id.nav_cards && activeUser.getWallet().size() != 0){
            Intent startNewActivity = new Intent(this, CardSwipeContainer.class);
            startActivity(startNewActivity);
        } else {
            goToFragment(id, new Bundle());
        }

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void goToFragment(final int id, final Bundle bundle) {

        if (id == R.id.nav_cards ) {
            goToCardSwipeScreen();

        } else if (id == R.id.nav_payments) {
            goToPaymentScreen();

        } else if (id == R.id.nav_booking) {
            goToBooking();

        }
    }


    private void goToPaymentScreen() {
        addFragment(PaymentsScreen.newInstance());
    }


    private void goToBooking() {
        addFragment(BookingScreen.newInstance());
    }


    private void goToCardSwipeScreen() {
        addFragment(CardSwipeScreen.newInstance());
    }


    private <T extends Fragment> void addFragment(final T fragment) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final String fragmentName = fragment.getClass().getName();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack( fragmentName )
                .commit();
    }


    public void flyScreen(View view) {
        Intent startNewActivity = new Intent(this, Fly.class);
        startActivity(startNewActivity);
    }


    public void driveScreen(View view) {
        Intent startNewActivity = new Intent(this, Drive.class);
        startActivity(startNewActivity);
    }


    public void sleepScreen(View view) {
        Intent startNewActivity = new Intent(this, Sleep.class);
        startActivity(startNewActivity);
    }


    public void cardLookup(View view) {
        Intent startNewActivity = new Intent(this, CardLookup.class);
        startActivity(startNewActivity);
    }


    public void sendSyPI(View view) {
        Intent startNewActivity = new Intent(this, SyPI.class);
        startActivity(startNewActivity);
    }


    public void addCard(View view) {
        Intent startNewActivity = new Intent(this, AddCard.class);
        startActivity(startNewActivity);
    }

    public void sendRewards(View view) {
        Intent startNewActivity = new Intent(this, Rewards.class);
        startActivity(startNewActivity);
    }
    //end onClick methods

}
