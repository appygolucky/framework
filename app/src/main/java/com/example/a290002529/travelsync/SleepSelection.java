package com.example.a290002529.travelsync;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class SleepSelection extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public User u;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sleep_selection);
        u = AccountManager.INSTANCE.getUser();

        ArrayList<HotelObject> hotelList = new ArrayList<>();
        hotelList.add(new HotelObject(300, "H111", "Hilton", 435, 1844, "Miami, Florida",4,
                ContextCompat.getDrawable(this, R.mipmap.hiltontest)));
        hotelList.add(new HotelObject(500, "H2222","Marriot", 730, 1453, "Miami, Florida",  8,
                ContextCompat.getDrawable(this, R.mipmap.marriotttest)));
        hotelList.add(new HotelObject(250, "H333","Ritz Carlton", 930, 1650, "Miami, FL", 3 ,
                ContextCompat.getDrawable(this, R.mipmap.ritztest)) );
        hotelList.add(new HotelObject(540, "H444", "Days Inn", 530, 1930, "Miami, FL", 8,
                ContextCompat.getDrawable(this, R.mipmap.daysinntest)) );
        hotelList.add(new HotelObject(600,"H555", "Residents Inn" , 1630, 2030, "Miami, FL", 4,
                ContextCompat.getDrawable(this, R.mipmap.residencetest)) );
        hotelList.add(new HotelObject(110, "H666", "American Inn", 230, 1230, "Miami, FL ", 4,
                ContextCompat.getDrawable(this, R.mipmap.americaninn) ));
        hotelList.add(new HotelObject(300, "H777", "Holiday Inn", 600, 1830,"Miami, FL", 6,
               ContextCompat.getDrawable(this, R.mipmap.holidayinntest)) );

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(hotelList);
        mRecyclerView.setAdapter(mAdapter);



    }


    //send to cart screen onClick
    public void sleepCart(View view) {
        final HotelObject selectedHotel = mAdapter.getSelectedHotel();
        if (selectedHotel != null) {
            u.getItinerary().addItem(selectedHotel);
        }

        showHotelPopUp();
    }

    private void showHotelPopUp(){
        AlertDialog.Builder helpbuilder = new AlertDialog.Builder(this);
        helpbuilder.setTitle("Your Hotel Has Been Successfully Added to Your Itinerary!");
        helpbuilder.setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent startNewActivity = new Intent(context, Cart.class);
                startActivity(startNewActivity);
            }
        });
        helpbuilder.setNegativeButton("Continue Booking", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //NOTE: Needs to be changed to send to CardsScreen instead of Booking
                Intent startNewActivity = new Intent(context, BaseNav.class);
                startActivity(startNewActivity);
            }
        });
        AlertDialog helpdialog = helpbuilder.create();
        helpdialog.show();
    }
}
