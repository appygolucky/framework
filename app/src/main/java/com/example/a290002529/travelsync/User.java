package com.example.a290002529.travelsync;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Random;

public class User implements Parcelable {

    // name of user
    private String name;
    // username of user
    private String username;
    // password for user
    private String password;
    // reward value for user
    private int rewards;

    // number of user cards
    private int cardCount;
    // itinenary for user
    private Itinerary itinerary;


    private ArrayList<Card> wallet = new ArrayList();

    /**
     * Blank Constructor for the User object
     *
     * Initializes no variables
     */
    public User()
    {
        this.itinerary = new Itinerary();
    }


    /**
     * Constructor for the User object
     *
     * @param name name for the user
     * @param username username for the user
     * @param password password for the user
     * @param rewards rewards for the user
     */
    public User(String name, String username, String password, int rewards, int cardCount)
    {
        this.name = name;
        this.username = username;
        this.password = password;
        this.rewards = rewards;
        this.itinerary = new Itinerary();
        this.cardCount = cardCount;
        for(int i = 0; i < cardCount; i++) {
            Double rand = Math.floor((Math.random()*500));
            int rewardValue = rand.intValue();
            Card c = new Card(i, 0, 5000, rewardValue);
            this.wallet.add(c);
        }
    }

    /**
     * Getter for user's name
     * @return name of user
     */
    public String getName()
    {
        return name;
    }

    /**
     * Getter for user's username
     * @return username of user
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * Getter for user's password
     * @return password of user
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * Getter for number of cards
     * @return number of cards of user
     */
    public int getCardCount()
    {
        return cardCount;
    }

    /**
     * Getter for user's name
     * @return name of user
     */
    public int getRewards()
    {
        return rewards;
    }

    /**
     * Method to add to user's rewards
     * @param value value to add to reward
     * @return new reward amount
     */
    public int addRewards(int value)
    {
        return value + rewards;
    }

    /**
     * Method to subtract from user's rewards
     * @param value value to subtract from reward
     * @return new reward amount
     */
    public int subtractRewards(int value)
    {
        return value - rewards;
    }

    /**
     * Method to get card object from user's wallet
     * @param value value to specify card location
     * @return selected card
     */
    public Card getCard(int value) {return wallet.get(value);}

    /**
     * Method to add card object to user's wallet
     * @param card card being added to wallet
     */
    public void addCard(Card card) {
        wallet.add(card);
    }

    /**
     * Method to get user's wallet
     * @return wallet ArrayList
     */
    public ArrayList<Card> getWallet() { return wallet; }

    public Itinerary getItinerary() {
        return itinerary;
    }

    //public void addItinerary(CarObject carObject) {itinerary.add(carObject)}


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.username);
        dest.writeString(this.password);
        dest.writeInt(this.rewards);
        dest.writeInt(this.cardCount);
        dest.writeParcelable(this.itinerary, flags);
        dest.writeTypedList(this.wallet);
    }

    protected User(Parcel in) {
        this.name = in.readString();
        this.username = in.readString();
        this.password = in.readString();
        this.rewards = in.readInt();
        this.cardCount = in.readInt();
        this.itinerary = in.readParcelable(Itinerary.class.getClassLoader());
        this.wallet = in.createTypedArrayList(Card.CREATOR);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
