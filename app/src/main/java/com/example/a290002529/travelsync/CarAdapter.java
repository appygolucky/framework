package com.example.a290002529.travelsync;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;


public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {


    private ArrayList<CarObject> mDataset;
    private RadioButton lastCheckedRadioButton = null;
    private int lastCheckedPosition = -1;


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RadioButton car_radButton;
        public TextView car_info;
        public ImageView car_icon;

        public ViewHolder(View v) {
            super(v);
            car_icon = (ImageView) v.findViewById(R.id.car_logo);
            car_info = (TextView) v.findViewById(R.id.car_info);
            car_radButton = (RadioButton) v.findViewById(R.id.car_radButton);
        }
    }


    public CarAdapter(ArrayList<CarObject> myDataset) {
        mDataset = myDataset;
    }


    public void add(int position, CarObject item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }


    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }


    public CarObject getSelectedCar() {
        if (lastCheckedPosition > -1) {
            return mDataset.get(lastCheckedPosition);
        }

        return null;
    }


    @Override
    public CarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_recycler_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        CarAdapter.ViewHolder vh = new CarAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(CarAdapter.ViewHolder holder, final int position) {
        final CarObject car = mDataset.get(position);

        // TODO - GET / SET CAR INFO
        String carDescription = "$ " + car.getPrice()+ "\n"
                + car.getMake() + " " + car.getModel() + "\n" ;
        holder.car_info.setText(carDescription);

        // TODO - GET CAR ICON
        holder.car_icon.setImageDrawable(car.getCarIcon());


        holder.car_radButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final boolean lastPositionWasChecked = (lastCheckedRadioButton != null);
                if (lastPositionWasChecked) {
                    lastCheckedRadioButton.setChecked(false);
                }

                final RadioButton clickedRadioButton = ((RadioButton) v);
                clickedRadioButton.setChecked(true);
                lastCheckedRadioButton = clickedRadioButton;
                lastCheckedPosition = position;

            }
        });

    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}