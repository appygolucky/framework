package com.example.a290002529.travelsync;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class FlySelection extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private FlightAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private User u;
    private FlightAdapter flightAdapter;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fly_selection);

        ArrayList<Trip> flightList = TripManager.INSTANCE.getTrip();

        mRecyclerView = (RecyclerView) findViewById(R.id.fly_recycler);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        flightAdapter = new FlightAdapter(flightList);
        mRecyclerView.setAdapter(flightAdapter);
    }

    //send to cart screen onClick
    public void flyCart(View view) {
        u = AccountManager.INSTANCE.getUser();
        final Trip flightObject = flightAdapter.getSelectedFlight();
        if (flightObject != null) {
            u.getItinerary().addItem(flightObject);
        }
        showFlightPopUp();
    }

    private void showFlightPopUp(){
        AlertDialog.Builder helpbuilder = new AlertDialog.Builder(this);
        helpbuilder.setTitle("Your Flight Has Been Successfully Added to Your Itinerary!");
        helpbuilder.setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent startNewActivity = new Intent(context, Cart.class);
                startActivity(startNewActivity);
            }
        });
        helpbuilder.setNegativeButton("Continue Booking", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //NOTE: Needs to be changed to send to CardsScreen instead of Booking
                Intent startNewActivity = new Intent(context, BaseNav.class);
                startActivity(startNewActivity);
            }
        });
        AlertDialog helpdialog = helpbuilder.create();
        helpdialog.show();
    }
}