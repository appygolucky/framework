package com.example.a290002529.travelsync;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Calendar;

import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;


public class Drive extends AppCompatActivity {
    public User u;
    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive);

        ImageButton returnImageBtn = (ImageButton) findViewById(R.id.imageButton);
        final EditText returnEditText = (EditText) findViewById(R.id.editText4);
        final CalendarDatePickerDialogFragment.OnDateSetListener returnOnDateSetListener = new CalendarDatePickerDialogFragment.OnDateSetListener() {
            @Override
            public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
            returnEditText.setText(monthOfYear+1 + "/" + dayOfMonth + "/" + year);
            }
        };
        returnImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOL","WHY U NO WORK?!");
                CalendarDatePickerDialogFragment returnCDP = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(returnOnDateSetListener)
                        .setFirstDayOfWeek(Calendar.SUNDAY);
                returnCDP.show(getSupportFragmentManager(), "RETURN_CDP");

            }
        });

        ImageButton pickupImageBtn = (ImageButton) findViewById(R.id.imageButton3);
        final EditText pickupEditText = (EditText) findViewById(R.id.selectDatePickup);
        final CalendarDatePickerDialogFragment.OnDateSetListener pickupOnDateSetListener = new CalendarDatePickerDialogFragment.OnDateSetListener() {
            @Override
            public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
                pickupEditText.setText(monthOfYear+1 + "/" + dayOfMonth + "/" + year);
            }
        };
        pickupImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOL","WHY U NO WORK?!");
                CalendarDatePickerDialogFragment pickupCDP = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(pickupOnDateSetListener)
                        .setFirstDayOfWeek(Calendar.SUNDAY);
                pickupCDP.show(getSupportFragmentManager(), "PICKUP_CDP");

            }
        });

        u = AccountManager.INSTANCE.getUser();


    }

    public void driveSelection(View view) {
        Intent startNewActivity = new Intent(this, DriveSelection.class);
        startActivity(startNewActivity);
    }



//    @SuppressWarnings("deprecation")
//    public void setDate(View view) {
//        showDialog(999);
//        Toast.makeText(getApplicationContext(), "ca",
//                Toast.LENGTH_SHORT)
//                .show();
//    }
//
//    @Override
//    protected Dialog onCreateDialog(int id) {
//        // TODO Auto-generated method stub
//        if (id == 999) {
//            return new DatePickerDialog(this,
//                    myDateListener, year, month, day);
//        }
//        return null;
//    }
//
//    private DatePickerDialog.OnDateSetListener myDateListener = new
//            DatePickerDialog.OnDateSetListener() {
//                @Override
//                public void onDateSet(DatePicker arg0,
//                                      int arg1, int arg2, int arg3) {
//                    // TODO Auto-generated method stub
//                    // arg1 = year
//                    // arg2 = month
//                    // arg3 = day
//                    showDate(arg1, arg2+1, arg3);
//                }
//            };
//
//    private void showDate(int year, int month, int day) {
//        dateView.setText(new StringBuilder().append(day).append("/")
//                .append(month).append("/").append(year));
  //  };



        //send to drive selection screen onClick
//    public void sendDriveSelection(View view){
//        Intent startNewActivity = new Intent(this, DriveSelection.class);
//        startActivity(startNewActivity);
//        //CarObject hondaAccord = new CarObject(200, "C1111", 0304,  );
//    }
}
