package com.example.a290002529.travelsync;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;

import java.util.Calendar;

public class Sleep extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sleep);

        ImageButton checkInBtn = (ImageButton) findViewById(R.id.checkInBtn);
        final EditText returnEditText = (EditText) findViewById(R.id.checkIn);
        final CalendarDatePickerDialogFragment.OnDateSetListener checkInOnDateSetListener = new CalendarDatePickerDialogFragment.OnDateSetListener() {
            @Override
            public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
                returnEditText.setText(monthOfYear+1 + "/" + dayOfMonth + "/" + year);
            }
        };
        checkInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOL","WHY U NO WORK?!");
                CalendarDatePickerDialogFragment checkInCDP = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(checkInOnDateSetListener)
                        .setFirstDayOfWeek(Calendar.SUNDAY);
                checkInCDP.show(getSupportFragmentManager(), "CHECKIN_CDP");

            }
        });



        ImageButton checkOutBtn = (ImageButton) findViewById(R.id.checkOutBtn);
        final EditText checkOutEditText = (EditText) findViewById(R.id.checkOut);
        final CalendarDatePickerDialogFragment.OnDateSetListener checkOutOnDateSetListener = new CalendarDatePickerDialogFragment.OnDateSetListener() {
            @Override
            public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
                checkOutEditText.setText(monthOfYear+1 + "/" + dayOfMonth + "/" + year);
            }
        };
        checkOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOL","WHY U NO WORK?!");
                CalendarDatePickerDialogFragment checkOutCDP = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(checkOutOnDateSetListener)
                        .setFirstDayOfWeek(Calendar.SUNDAY);
                checkOutCDP.show(getSupportFragmentManager(), "CHECKOUT_CDP");

            }
        });
    }

    //send to sleep selection screen onClick
    public void sendSleepSelection(View view){
        Intent startNewActivity = new Intent(this, SleepSelection.class);
        startActivity(startNewActivity);


    }
}
