package com.example.a290002529.travelsync;

import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class CreditFlexing extends AppCompatActivity {

    public User u;
    private double cathatyLimit;
    private double hertzLimit;
    private double stashLimit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_flexing);

        u = AccountManager.INSTANCE.getUser();
        u.getWallet().get(1).addLimit(1000);
        u.getWallet().get(2).subtractLimit(1000);

    }

    public void transferAction(View view){
        Intent startNewActivity = new Intent(this, Cart.class);
        startActivity(startNewActivity);
    }
}
