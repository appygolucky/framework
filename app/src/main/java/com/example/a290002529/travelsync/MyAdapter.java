package com.example.a290002529.travelsync;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private ArrayList<HotelObject> mDataset;

    private RadioButton lastCheckedRadioButton = null;
    private int lastSelectedPosition = -1;

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RadioButton hotel_radButton;
        public TextView hotel_info;
        public ImageView hotel_icon;

        public ViewHolder(View v) {
            super(v);
            hotel_icon = (ImageView) v.findViewById(R.id.hotel_logo);
            hotel_info = (TextView) v.findViewById(R.id.hotel_info);
            hotel_radButton = (RadioButton) v.findViewById(R.id.hotel_radButton);
            //hotel = (TextView) v.findViewById(R.id.firstLine);
            //txtFooter = (TextView) v.findViewById(R.id.secondLine);
        }
    }

    public void add(int position, HotelObject item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(ArrayList<HotelObject> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        // - get element from your dataset at this position
        // - replace the contents of the view with that element
       final HotelObject hotel = mDataset.get(position);
        String hotelDescription = "$ " + hotel.getPrice()+ "\n" + hotel.getHotelLocation() + "\n" + hotel.getName() ;
        holder.hotel_info.setText(hotelDescription);
        holder.hotel_icon.setImageDrawable(hotel.getHotelImage());
        holder.hotel_radButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                final boolean lastPositionWasChecked = (lastCheckedRadioButton != null);
                if (lastPositionWasChecked) {
                    lastCheckedRadioButton.setChecked(false);
                }

                final RadioButton clickedRadioButton = ((RadioButton) v);
                clickedRadioButton.setChecked(true);
                lastCheckedRadioButton = clickedRadioButton;
                lastSelectedPosition = position;

            }
        });
    }


    public HotelObject getSelectedHotel() {
        if (lastSelectedPosition > -1) {
            return mDataset.get(lastSelectedPosition);
        }

        return null;
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}

