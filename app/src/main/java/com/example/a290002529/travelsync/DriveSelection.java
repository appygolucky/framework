package com.example.a290002529.travelsync;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class DriveSelection extends AppCompatActivity {

    private ArrayList<CarObject> cars;
    public User u;

    private RecyclerView mRecyclerView;
    private CarAdapter carAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive_slection);

        cars = new ArrayList<>();
        cars.add(new CarObject(600, "carPorsche", 1206, 1209, "918 Spyder", "Porsche", 2015,
                ContextCompat.getDrawable(this, R.mipmap.porshe)));
        cars.add(new CarObject(750, "carMcLaren", 1206, 1209, "P1", "McLaren", 2016,
                ContextCompat.getDrawable(this, R.mipmap.mclaren)));
        cars.add(new CarObject(800, "carFerrari", 1206, 1209, "LaFerrari", "Ferrari", 2015 ,
                ContextCompat.getDrawable(this, R.mipmap.ferari)));
        cars.add(new CarObject(950, "carKoenigsegg", 1206, 1209, "Agera One:1", "Koenigsegg", 2015,
                ContextCompat.getDrawable(this, R.mipmap.konneseigg)));
        cars.add(new CarObject(450, "carMaserati", 1206, 1209, "MC12", "Maserati", 2005,
                ContextCompat.getDrawable(this, R.mipmap.maserati)));
        cars.add(new CarObject(700, "carNissan", 1206, 1209, "GTR", "Nissan", 2017,
                ContextCompat.getDrawable(this, R.mipmap.nissan_gtr)));
        cars.add(new CarObject(500, "carBMW", 1206, 1209, "M6", "BMW", 2016,
                ContextCompat.getDrawable(this, R.mipmap.bmw)));


        setupRecycler();

        u = AccountManager.INSTANCE.getUser();

    }


    private void setupRecycler() {
        mRecyclerView = (RecyclerView) findViewById(R.id.drive_recycler);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        carAdapter = new CarAdapter(cars);
        mRecyclerView.setAdapter(carAdapter);
    }


    public void onAddCarItinerary (View view){
        final CarObject carObject = carAdapter.getSelectedCar();
        if (carObject != null) {
            u.getItinerary().addItem(carObject);
        }

        showCarPopUp();

    }

    private void showCarPopUp(){
        AlertDialog.Builder helpbuilder = new AlertDialog.Builder(this);
        helpbuilder.setTitle("Your Rental Car Has Been Successfully Added to Your Itinerary!");
        helpbuilder.setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent startNewActivity = new Intent(context, Cart.class);
                startActivity(startNewActivity);
            }
        });
        helpbuilder.setNegativeButton("Continue Booking", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //NOTE: Needs to be changed to send to CardsScreen instead of Booking
                Intent startNewActivity = new Intent(context, BaseNav.class);
                startActivity(startNewActivity);
            }
        });
        AlertDialog helpdialog = helpbuilder.create();
        helpdialog.show();
    }
}
