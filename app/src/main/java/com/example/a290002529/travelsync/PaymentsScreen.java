package com.example.a290002529.travelsync;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by 290002529 on 11/4/2016.
 */

public class PaymentsScreen extends Fragment {

    View myView;

    public static PaymentsScreen newInstance() {
        return new PaymentsScreen();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.payments , container, false);
        return myView;
    }

}
