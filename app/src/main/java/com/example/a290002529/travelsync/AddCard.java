package com.example.a290002529.travelsync;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.a290002529.travelsync.Api.PARAMS;

import java.util.ArrayList;
import java.util.Random;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

public class AddCard extends AppCompatActivity implements PARAMS {

    public String TAG = getClass().getName();
    private Button btnAddFromCamera;
    private int MY_SCAN_REQUEST_CODE = 100; //arbitrary int
    private TextView resultTextView;
    public String redactedCard;
    public User user;
    public String lastDigits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);


        btnAddFromCamera = (Button) findViewById(R.id.btnAddFromCamera);
        resultTextView = (TextView) findViewById(R.id.resultTextView);

        user = AccountManager.INSTANCE.getUser();
    }

    public void addCardFromCamera(View view){
        Intent scanIntent = new Intent(this, CardIOActivity.class);
        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_RESTRICT_POSTAL_CODE_TO_NUMERIC_ONLY, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, false); // default: false

        // hides the manual entry button
        // if set, developers should provide their own manual entry mechanism in the app
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default: false

        // matches the theme of your application
        scanIntent.putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME, false); // default: false

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    private void showCardPopUp(){
        AlertDialog.Builder helpbuilder = new AlertDialog.Builder(this);
        helpbuilder.setTitle("Card Successfully Added");
        helpbuilder.setMessage("Credit card ending in " + lastDigits + " has been added to your mobile wallet.");
        helpbuilder.setPositiveButton("Add Another Card", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //close back to card activity
            }
        });
        helpbuilder.setNegativeButton("Back to Mobile Wallet", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //NOTE: Needs to be changed to send to CardsScreen instead of Booking
                Intent startNewActivity = new Intent(getApplicationContext(), CardSwipeContainer.class);
                startActivity(startNewActivity);
            }
        });
        AlertDialog helpdialog = helpbuilder.create();
        helpdialog.show();
    }

    private void showCancelPopUp(){
        AlertDialog.Builder helpbuilder = new AlertDialog.Builder(this);
        helpbuilder.setTitle("Card Scan Cancelled");
        helpbuilder.setMessage("Your card was not successfully added to your mobile wallet.");
        helpbuilder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //close back to card activity
            }
        });
        AlertDialog helpdialog = helpbuilder.create();
        helpdialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String resultStr;
        if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
            CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

            // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
            resultStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";
            redactedCard = scanResult.getLastFourDigitsOfCardNumber();

            // Do something with the raw number, e.g.:
            // myService.setCardNumber( scanResult.cardNumber );

            if (scanResult.isExpiryValid()) {
                resultStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
            }

            if (scanResult.cvv != null) {
                // Never log or display a CVV
                resultStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
            }

            if (scanResult.postalCode != null) {
                resultStr += "Postal Code: " + scanResult.postalCode + "\n";
            }

            if (scanResult.cardholderName != null) {
                resultStr += "Cardholder Name : " + scanResult.cardholderName + "\n";
            }

            Random random = new Random();
            final Card newCard = new Card(3, 0, 5000, (random.nextInt(1000)+1));
            AccountManager.INSTANCE.getUser().addCard(newCard);


            lastDigits = scanResult.getLastFourDigitsOfCardNumber();

            showCardPopUp();

        } else {
            showCancelPopUp();
        }



    }


    public void sendCardSelection (View view){
        Intent startNewActivity = new Intent(this, CardLookup.class);
        startActivity(startNewActivity);

    }



}
