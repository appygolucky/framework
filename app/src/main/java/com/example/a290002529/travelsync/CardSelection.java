package com.example.a290002529.travelsync;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Random;

public class CardSelection extends AppCompatActivity {

    public float y = 0;
    public User u;
    public ArrayList<Card> cards;
    public int limit = 500;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_selection);

        u = AccountManager.INSTANCE.getUser();

        cards = new ArrayList<Card>();

        for (int i = 0; i < 3; i++) {
            Random random = new Random();
            Card c = new Card(i, 0, limit, (random.nextInt(1000)+1));
            cards.add(c);
            u.getWallet().add(c);
            limit = limit + 2000;
        }
        

        for (Card card : cards){
            y = y + 375;
            ImageView imageview = new ImageView(CardSelection.this);
            RadioButton radioButton = new RadioButton(CardSelection.this);
            EditText editText = new EditText(CardSelection.this);
            RelativeLayout relativelayout = (RelativeLayout) findViewById(R.id.activity_card_selection);
            Bitmap bMap = BitmapFactory.decodeResource(getResources(), card.getFile());
            Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, 550, 350, true);
            imageview.setImageBitmap(bMapScaled);
            imageview.setY(y);
            imageview.setX(0);
            imageview.setBackgroundColor(getResources().getColor(R.color.bpDark_gray));
            radioButton.setY(y+130);
            radioButton.setX(900);

            ColorStateList colorStateList = new ColorStateList(
                    new int[][]{
                            new int[]{-android.R.attr.state_checked},
                            new int[]{android.R.attr.state_checked}
                    },
                    new int[]{

                            R.color.bpWhite
                            , R.color.colorPrimaryDark
                    }
            );
            radioButton.setBackgroundTintList(colorStateList);

            editText.setY(y+80);
            editText.setX(625);
            editText.setHint("cvv");
            editText.setHintTextColor(getResources().getColor(R.color.lightGray));
            editText.setTextColor(getResources().getColor(R.color.bpWhite));
            editText.setTextSize(30f);
            relativelayout.addView(imageview);
            relativelayout.addView(radioButton);
            relativelayout.addView(editText);


        }
    }



    public void cardsHome(View view) {
        Intent startNewActivity = new Intent(this, CardSwipeContainer.class);
        startActivity(startNewActivity);


        /**Intent startNewActivity = new Intent(this, BaseNav.class);
        Bundle b = new Bundle();
        b.putInt("key", 0); //Your id
        startNewActivity.putExtras(b); //Put your id to your next Intent
        startActivity(startNewActivity);
        finish();
         */
    }
}
