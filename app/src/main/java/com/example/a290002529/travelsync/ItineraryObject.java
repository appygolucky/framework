package com.example.a290002529.travelsync;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 290002528 on 11/29/2016.
 */

public class ItineraryObject implements Parcelable{
    public double price;
    private String id;
    public String model;
    public String make;

    public ItineraryObject() {
        // empty constructor
    }

    public ItineraryObject(double price, String id) {
        this.price = price;
        this.id = id;
    }



    public double getPrice()
    {
        return price;
    }

    public String getId()
    {
        return id;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public void setId(String id){
    this.id=id;
    }

    //public void getId(String id)
    {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.price);
        dest.writeString(this.id);
    }

    protected ItineraryObject(Parcel in) {
        this.price = in.readDouble();
        this.id = in.readString();
    }

    public static final Creator<ItineraryObject> CREATOR = new Creator<ItineraryObject>() {
        @Override
        public ItineraryObject createFromParcel(Parcel source) {
            return new ItineraryObject(source);
        }

        @Override
        public ItineraryObject[] newArray(int size) {
            return new ItineraryObject[size];
        }
    };
}