package com.example.a290002529.travelsync;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import static com.example.a290002529.travelsync.Api.PARAMS.USER_KEY;

public class Cart extends AppCompatActivity {
    public User u;
    public int y;
    public Itinerary itinerary;
    public ArrayList<ItineraryObject> itineraryList;
    public double totalPrice = 0;
    private Context context = this;
    private double flightCost = 0;
    private double hotelCost = 0;
    private double carCost = 0;
    private ArrayList<Double> costs = new ArrayList<Double>();
    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        u = AccountManager.INSTANCE.getUser();
        itinerary = u.getItinerary();
        itineraryList = itinerary.getItems();


        for (ItineraryObject itineraryObject : itineraryList) {
            y = y + 300;

            if (itineraryObject instanceof CarObject) {
                TextView textView = new TextView(Cart.this);
                ImageView imageView = new ImageView(Cart.this);
                imageView.setMaxWidth(80);
                imageView.setMaxHeight(80);

                double carPrice = itineraryObject.getPrice();
                //int rentDuration = ((CarObject) itineraryObject).getReturnDate() - ((CarObject) itineraryObject).getRentalDate();
                carCost = carPrice * 5;
                totalPrice = totalPrice + carCost;


                int carYear = ((CarObject) itineraryObject).getYear();

                String priceString = String.format("%.2f", carPrice);
                String make = ((CarObject) itineraryObject).getMake();
                String model = ((CarObject) itineraryObject).getModel();
                String year = Integer.toString(carYear);

                imageView.setImageResource(R.drawable.car);
                imageView.setY(y+80);
                imageView.setX(0);

                textView.setText(year + " " + make + " " + model + "\nPrice per Day: $"+ priceString);
                textView.setY(y+80);
                textView.setX(300);
                textView.setTextColor(getResources().getColor(R.color.bpWhite));
                textView.setTextSize(22f);


                RelativeLayout relativelayout = (RelativeLayout) findViewById(R.id.activity_cart);
                //relativelayout.addView(imageView);
                relativelayout.addView(textView);
            }

            if (itineraryObject instanceof HotelObject) {
                TextView textView = new TextView(Cart.this);
                ImageView imageView = new ImageView(Cart.this);
                imageView.setMaxWidth(80);
                imageView.setMaxHeight(80);

                double hotelPrice = itineraryObject.getPrice();
                hotelCost = hotelPrice * 5;
                totalPrice = totalPrice + hotelCost;

                String priceString = String.format("%.2f", hotelPrice);
                String name = ((HotelObject) itineraryObject).getName();
                String location = ((HotelObject) itineraryObject).getHotelLocation();


                imageView.setImageResource(R.drawable.bed);
                imageView.setY(y+80);
                imageView.setX(0);


                textView.setText(name + " " + location + "\nPrice per Night: $"+ priceString);
                textView.setY(y+80);
                textView.setX(300);
                textView.setTextColor(getResources().getColor(R.color.bpWhite));
                textView.setTextSize(22f);

                RelativeLayout relativelayout = (RelativeLayout) findViewById(R.id.activity_cart);
                //relativelayout.addView(imageView);
                relativelayout.addView(textView);
            }

            if (itineraryObject instanceof Trip) {
                TextView textView = new TextView(Cart.this);
                ImageView imageView = new ImageView(Cart.this);
                imageView.setMaxWidth(80);
                imageView.setMaxHeight(80);

                double flightPrice =  itineraryObject.getPrice();
                flightCost = flightPrice;
                totalPrice = totalPrice + flightCost;

                String priceString = String.format("%.2f", flightPrice);
                String airline = ((Trip) itineraryObject).getCarrier();
                String departure = ((Trip) itineraryObject).getOrigin();
                String arrival = ((Trip) itineraryObject).getDestination();

                imageView.setImageResource(R.drawable.bed);
                imageView.setY(y+80);
                imageView.setX(0);


                textView.setText(airline + "\n" + "Departure Location: " + departure + "   Arrival Location: "  + arrival + "\nPrice of Flight: $"+ priceString);
                textView.setY(y+80);
                textView.setX(300);
                textView.setTextColor(getResources().getColor(R.color.bpWhite));
                textView.setTextSize(22f);

                RelativeLayout relativelayout = (RelativeLayout) findViewById(R.id.activity_cart);
                //relativelayout.addView(imageView);
                relativelayout.addView(textView);
            }

        }
        String totalPriceString = String.format("%.2f", totalPrice);
        TextView totalPriceView = (TextView) findViewById(R.id.tvTotalPrice);
        totalPriceView.setText("Total Trip Cost: $" + totalPriceString);

        costs.add(flightCost);
        costs.add(carCost);
        costs.add(hotelCost);


    }


    //send to booking screen onClick
    public void bookingScreen(View view){
        Intent startNewActivity = new Intent(this, BaseNav.class);
        startActivity(startNewActivity);
    }

    //send to flex screen onClick
    public void sendCheckout (View view){

        if(u.getWallet().get(0).getLimit() < flightCost) {
            showFlexPopUp();
        }else if(u.getWallet().get(1).getLimit() < carCost) {
            showFlexPopUp();
        }else if(u.getWallet().get(2).getLimit() < hotelCost) {
            showFlexPopUp();
        }else {
            for (Double costItem : costs) {
                u.getWallet().get(counter).addBalance(costItem);
                u.getWallet().get(counter).addRewards((int) Math.round(costItem * 0.05));
                counter = counter + 1;
            }
            showConfirmPopUp();
        }
    }

    private void showFlexPopUp(){
        AlertDialog.Builder helpbuilder = new AlertDialog.Builder(this);
        helpbuilder.setTitle("Credit Flexing");
        helpbuilder.setMessage("It looks like one of your itinerary objects exceeds your available credit.  Would you like to flex credit from another card?");
        helpbuilder.setPositiveButton("Credit Flexing", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent startNewActivity = new Intent(context, CreditFlexing.class);
                startActivity(startNewActivity);
            }
        });
        helpbuilder.setNegativeButton("Return to Cart", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog helpdialog = helpbuilder.create();
        helpdialog.show();
    }

    private void showConfirmPopUp(){
        AlertDialog.Builder helpbuilder = new AlertDialog.Builder(this);
        helpbuilder.setTitle("Success");
        helpbuilder.setMessage("Your trip has been booked!");
        helpbuilder.setPositiveButton("Return to Mobile Wallet", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent startNewActivity = new Intent(context, CardSwipeContainer.class);
                startActivity(startNewActivity);
            }
        });
        helpbuilder.setNegativeButton("Return to Booking", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent startNewActivity = new Intent(context, BaseNav.class);
                startActivity(startNewActivity);
            }
        });
        AlertDialog helpdialog = helpbuilder.create();
        helpdialog.show();
    }


}
