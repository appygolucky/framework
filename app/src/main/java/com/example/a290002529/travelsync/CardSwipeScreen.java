package com.example.a290002529.travelsync;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class CardSwipeScreen extends Fragment {

    public static CardSwipeScreen newInstance() {
        return new CardSwipeScreen();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cardscreen, container, false);
    }


    @RequiresApi(api = Build.VERSION_CODES.DONUT)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ViewPager swipeViewPager = (ViewPager) view.findViewById(R.id.viewPager);

        final User currentUser = AccountManager.INSTANCE.getUser();

        final SwipePagerAdapter swipePagerAdapter = new SwipePagerAdapter(getContext());
        swipeViewPager.setAdapter(swipePagerAdapter);
        swipePagerAdapter.setCards(currentUser.getWallet());
    }

}

